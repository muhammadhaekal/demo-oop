class Square {
    constructor(side) {
        this.side = side
    }

    area() {
        return this.side * this.side
    }

    perimeter() {
        return 4 * this.side
    }
}

const square1 = new Square(5)
const square2 = new Square(10)


console.log("square2 side:", square2.side)
console.log("square2 perimeter", square2.perimeter())

class Person {
    constructor(firstName, lastName, age) {
        this.firstName = firstName
        this.lastName = lastName
        this.age = age
    }

    greeting() {
        return `Hello my name is ${this.firstName} ${this.lastName}`
    }
}

class Teacher extends Person {
    constructor(firstName, lastName, age, school, title) {
        super(firstName, lastName, age)
        this.school = school
        this.title = title
    }

    displayFullNameAndTitle() {
        return `${this.firstName} ${this.lastName} ${this.title}`
    }
}

const evan = new Person("Michael", "Pannjang", 20)
const masAdi = new Teacher("Adi", "Sinambela", 21, "SMA Negri 4 Medan", "S.Pd")

console.log(evan.greeting())
console.log(masAdi.greeting())
console.log(masAdi.displayFullNameAndTitle())