class Cart {
    constructor() {
        this.products = []
    }

    addProduct(name, quantity) {
        let index = this.products.findIndex(
            (product) => {
                return product.name === name
            }
        )

        if (index === -1) {
            this.products.push({
                name: name,
                quantity: quantity
            })
        } else {
            this.products[index].quantity += quantity
        }

        // let index
        // let found = this.products.find((product, target) => {
        //     index = target
        //     return product.name === name
        // })

        // if (!found) {
        //     this.products.push({
        //         name: name,
        //         quantity: quantity
        //     })
        // } else {
        //     this.products[index].quantity += quantity
        // }
    }

    removeProduct(name) {
        let index = this.products.findIndex((product) => {
            return product.name === name
        })

        if (index !== -1) {
            this.products.splice(index, 1)
        }
    }

    showCart() {
        this.products.forEach((product) => {
            console.log(`${product.name} (${product.quantity})`)
        })
    }



}

const cart = new Cart()

cart.addProduct("White Cap", 2);

cart.addProduct("Red Shoes", 1);
cart.addProduct("Red Shoes", 4);
cart.addProduct("Red Shoes", 2);

cart.addProduct("Black Shirt", 3);
cart.removeProduct("Black Shirt");

cart.removeProduct("Green Jacket");

cart.showCart();