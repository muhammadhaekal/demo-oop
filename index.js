class Human {
    constructor(name, year_born) {
        this.name = name;
        this.year_born = year_born;
    }

    //Static Method
    static arms() {
        return 2
    }

    //Getter
    get getAgeDescription() {
        return `${this.name} age is ${this.calculateAge()} years old`
    }

    //Method
    calculateAge() {
        return 2018 - this.year_born
    }
}

class Employee extends Human {
    constructor(name, year_born, profession, salary) {
        super(name, year_born);
        this.profession = profession;
        this.salary = salary;
        this.calculateAgeAgain = this.calculateAgeAgain.bind(this)
    }

    updateSalary(salary) {
        this.salary = salary
    }

    calculateAgeAgain() {
        return this.calculateAge();
    }
}



const haidar = new Human("Haidar", 1993);
const fadhil = new Human("Fadhil", 2004);
const raffi = new Human("Raffi", 1992);

const budi = new Employee("Budi", 1945, "Senior Developer", 20000)
const untung = new Employee("Untung", 1990, "Junior Developer", 10000)

var budiAge = budi.calculateAgeAgain

console.log(budiAge())